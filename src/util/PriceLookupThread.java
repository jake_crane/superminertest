package util;

import superMiner.GemInfo;
import superMiner.OreInfo;
import superMiner.SuperMiner;
import methods.MyMethods;
import methods.PriceLookup;

public class PriceLookupThread extends Thread {

	private SuperMiner script;

	public PriceLookupThread(SuperMiner script) {
		this.script = script;
	}

	@Override
	public void run() {

		//try { 

			for (OreInfo oreInfo : script.oreInfoList()) {
				oreInfo.setPrice(PriceLookup.getPriceUsingRSBotGeItem(oreInfo.getId()));
				//DebugMethods.println("pricelookupdebug");
				if (oreInfo.getPrice() == 0) {
					MyMethods.println("price lookup failed for " +  oreInfo.getName());
					MyMethods.println("trying again...");
					oreInfo.setPrice(PriceLookup.getPrice(oreInfo.getId()));
				}
			}

			for (GemInfo gem : script.gems) {
				gem.setPrice(PriceLookup.getPriceUsingRSBotGeItem(gem.getId()));
				if (gem.getPrice() == 0) {
					MyMethods.println("price lookup failed for " +  gem.getName());
					MyMethods.println("trying again...");
					gem.setPrice(PriceLookup.getPrice(gem.getId()));
				}
			}

		//} catch (SecurityException s) {
			//System.err.println("SecurityException when looking up prices");
		//}

		/*SuperMiner.uncutSapphirePrice = PriceLookup.getScriptWithUsPrice(Gem.UNCUT_SAPPHIRE.getId());
		SuperMiner.uncutEmeraldPrice = PriceLookup.getScriptWithUsPrice(Gem.UNCUT_EMERALD.getId());
		SuperMiner.uncutRubyPrice = PriceLookup.getScriptWithUsPrice(Gem.UNCUT_RUBY.getId());
		SuperMiner.uncutDiamondPrice = PriceLookup.getScriptWithUsPrice(Gem.UNCUT_DIAMOND.getId());*/
	}
}