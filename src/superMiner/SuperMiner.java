package superMiner;

import generalTasks.Mine;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import methods.DebugMethods;
import methods.MyCalculations;
import methods.MyMethods;
import myAPI.SkillTracker;

import org.powerbot.script.MessageEvent;
import org.powerbot.script.MessageListener;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script.Manifest;
import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.GameObject;
import org.powerbot.script.rt6.Skills;

import util.PriceLookupThread;
import GUI.Gui;
import GUI.ShowMoreGui;
import areaInfo.AlKharid;
import areaInfo.BarbarianVillageBankEdgeville;
import areaInfo.DropOnly;
import areaInfo.FaladorUnderGroundBankFallyEast;
import areaInfo.LivingRockCaverns;
import areaInfo.LumbridgeSwampWest;
import areaInfo.MiningGuild;
import areaInfo.Rimmington;
import areaInfo.VarrockEast;
import areaInfo.VarrockWest;
import areaInfo.Yanille;

//banned words = "mineral", "Granite"
@Manifest(name = "Super Miner", 
description = "Supports banking and powermining for many locations. "
		+ "LRC "
		//+ "and Essence "
		+ "supported. Visit thread for details.",
		properties = "topic=1127043;")

public class SuperMiner extends PollingScript<ClientContext> implements PaintListener, MessageListener, MouseListener {

	private final double version = 2.1;

	private long startTime = 0;

	public static final int SHARK_ID = 385, SPIN_TICKET_ID = 24154, COALBAG_ID = 18339;

	public static final int[] ITEM_IDS_TO_KEEP = {SPIN_TICKET_ID, Pickaxe.IRON.getId(), Pickaxe.STEEL.getId(), 
		Pickaxe.MITHRIL.getId(), Pickaxe.ADAMANT.getId(), Pickaxe.RUNE.getId(), Pickaxe.DRAGON.getId(),
		Pickaxe.LENT_DRAGON.getId(), COALBAG_ID, SummoningItem.LAVA_TITAN_POUCH.getId(), SHARK_ID, 
		Potion.SUPER_RESTORE_1.getId(), Potion.SUPER_RESTORE_2.getId(), Potion.SUPER_RESTORE_3.getId(), 
		Potion.SUPER_RESTORE_4.getId(), Potion.SUMMONNG_1.getId(), Potion.SUMMONNG_2.getId(), 
		Potion.SUMMONNG_3.getId(), Potion.SUMMONNG_4.getId(), SummoningItem.UNKNOWN_SUMMONING_NECKLACE.getId(), 
		SummoningItem.UNKNOWN_SUMMONING_NECKLACE_2.getId()};

	public static final int[] strangerockIDs = {15532, 15533};

	public static final int[] UNCUT_STONE_IDS = {Gem.UNCUT_SAPPHIRE.getId(), 
		Gem.UNCUT_EMERALD.getId(), Gem.UNCUT_RUBY.getId(), Gem.UNCUT_DIAMOND.getId()};

	private String miningMethod = "Dropping";

	private String displayname;

	private boolean bankingEnabled;

	private boolean dropASAP = false;

	private boolean useLodestone = true;

	private boolean pickupOreOnGround;

	//private int pickaxeId;

	private String message;

	private boolean debug = false;

	public static final int HOME_TELEPORT_ANIMATION_1 = 16385;
	public static final int HOME_TELEPORT_ANIMATION_2 = 16386;
	public static final int HOME_TELEPORT_ANIMATION_3 = 16393;

	public int coalBagCount = 0;

	private ArrayList<OreInfo> oreInfoList = new ArrayList<OreInfo>();
	public GemInfo[] gems = new GemInfo[0];
	private ArrayList<Rock> rocks = new ArrayList<Rock>();

	private long lastCountedDeposits = System.currentTimeMillis() - 30000;

	private int moneyGained = 0;

	private static final Color PaintBackgroundColor = new Color(0, 0, 0, 180);

	private SkillTracker skillTracker = null;

	private ArrayList<Task> tasks = new ArrayList<Task>();

	private AreaInfo areaInfo = null;
	
	@Override
	public void start() {

		if (debug) {
			System.out.println("start");
		}

		if (!ctx.game.loggedIn()) {
			MyMethods.println("Please login before starting the script.");
			ctx.controller.stop();
			return;
		}

		if (ctx.backpack.collapsed()) {
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog(frame,
					"Please fully expand your backpack before starting the script.");
			frame.dispose();
			ctx.controller.stop();
			return;
		}

		displayname = ctx.players.local().name();

		Gui mygui = new Gui(ctx);
		mygui.setVisible(true);

		while (mygui.isVisible()) {
			MyMethods.sleep(1000, 1000);
		}

		mygui.dispose();

		if (debug) {
			System.out.print("rocks selected:");
			for (Rock rock : rocks) {
				System.out.println(rock.getName() + ", ");
			}
		}

		taskSetup();

		new PriceLookupThread(this).start();

		//pickaxeId = getInventoryPickaxeId();

		skillTracker = new SkillTracker(ctx, Skills.MINING);

		startTime = System.currentTimeMillis();

	}

	private void taskSetup() {

		ArrayList<AreaInfo> areaInfos = new ArrayList<AreaInfo>() {{

			add(new AlKharid(ctx));
			add(new BarbarianVillageBankEdgeville(ctx));
			add(new FaladorUnderGroundBankFallyEast(ctx));
			add(new LivingRockCaverns(ctx));
			add(new LumbridgeSwampWest(ctx));
			add(new MiningGuild(ctx));
			add(new Rimmington(ctx));
			add(new VarrockEast(ctx));
			add(new VarrockWest(ctx));
			add(new Yanille(ctx));

			add(new DropOnly(ctx));

		}};

		for (AreaInfo areaInfo : areaInfos) {
			if (areaInfo.shouldBeUsed()) {
				MyMethods.println(areaInfo.getClass().getSimpleName() + " selected");
				this.areaInfo = areaInfo;
				areaInfo.addTasks(tasks);
				break;
			}
		}

	}

	public String message() {
		return message;
	}

	public void message(String message) {
		this.message = message;
	}

	public void miningMethod(String method) {
		this.miningMethod = method;
	}

	public boolean pickupOreOnGround() {
		return pickupOreOnGround;
	}

	public void pickupOreOnGround(boolean newValue) {
		pickupOreOnGround = newValue;
	}

	public ArrayList<OreInfo> oreInfoList() {
		return oreInfoList;
	}

	public ArrayList<Rock> rocks() {
		return rocks;
	}

	public long startTime() {
		return startTime;
	}

	public boolean useLodestone() {
		return useLodestone;
	}

	public int moneyGained() {
		return moneyGained;
	}

	public boolean debug() {
		return debug;
	}

	public boolean bankingEnabled() {
		return bankingEnabled;
	}

	public void bankingEnabled(boolean newValue) {
		bankingEnabled = newValue;
	}

	public boolean dropASAP() {
		return dropASAP;
	}

	public String displayname() {
		return displayname;
	}

	public void dropASAP(boolean newValue) {
		dropASAP = newValue;
	}

	/*public int getInventoryPickaxeId() {

		final int[] PICKAXE_IDS = {Pickaxe.IRON.getId(), Pickaxe.STEEL.getId(), 
				Pickaxe.MITHRIL.getId(), Pickaxe.ADAMANT.getId(), Pickaxe.RUNE.getId(), 
				Pickaxe.DRAGON.getId(), Pickaxe.LENT_DRAGON.getId(),};

		for (Item i : ctx.backpack.select().id(PICKAXE_IDS)) {
			return i.id();
		}

		return -1;

	}*/

	/**
	 * 
	 * @return Returns the mining subScript that handles the area the local player is standing in.
	 * If the player is in an unknown area and has banking disabled, the default mining subscript will be returned.
	 * If the player is in an unknown area and has banking enabled, null will be returned.
	 */
	/*public MiningSubScript getMiningSubScriptForLocation() {

		ArrayList<MiningSubScript> subScripts = new ArrayList<MiningSubScript>();

		subScripts.add(new VarrockEastEssenceMining(ctx));
		subScripts.add(new VarrockWestMining(ctx));
		subScripts.add(new VarrockEastMining(ctx));
		subScripts.add(new RimmingtonMineMining(ctx));
		subScripts.add(new DesertQuarry(ctx));
		subScripts.add(new AlKharidDungeon(ctx));
		subScripts.add(new AlKharid(ctx));
		subScripts.add(new MineBarbarianBankEdgeville(ctx));
		subScripts.add(new MineFaladorUndergroundBankResourceDungeon(ctx));
		subScripts.add(new MineInMiningGuildBankResourceDungeon(ctx));
		subScripts.add(new MineInMiningGuildBankFallyEast(ctx));
		subScripts.add(new MineFaladorUnderGroundBankFallyEast(ctx));
		subScripts.add(new MineAndBankInResourceDungeon(ctx));
		subScripts.add(new Yanille(ctx));
		subScripts.add(new MineLumbySwampBankDraynor(ctx));
		subScripts.add(new LivingRockCaverns(ctx, skillTracker));
		subScripts.add(new GrandTree(ctx));
		subScripts.add(new CraftingGuild(ctx));
		subScripts.add(new Keldagrim(ctx));

		for (MiningSubScript miningSubScript : subScripts) {
			if (miningSubScript.shouldBeUsed()) {
				MyMethods.println("starting " + miningSubScript.getClass().getName());
				if (bankingEnabled) {
					MyMethods.println("banking enabled");
				}
				subScripts.clear();
				return miningSubScript;
			}
		}

		subScripts.clear();

		if (bankingEnabled) {
			message = "Unsupported banking Location";
			MyMethods.println(message);
			MyMethods.sleep(6000, 6000);
			ctx.controller.stop();
			return null;
		} 

		MyMethods.println("unknown mining location. Using default mining subscript.");
		MyMethods.println("starting DefaultMiningSubScript()");
		return new Default(ctx);

	}*/

	@Override
	public void poll() {

		if (debug) {
			DebugMethods.println("poll()");
		}

		try {

			/*if (ctx.game.getClientState() != Game.INDEX_MAP_LOADED
					&& ctx.game.getClientState() != Game.INDEX_LOBBY_SCREEN) {
				MyMethods.println("waiting for game to load...");
				return 1000;
			}*/

			//if (ctx.hud.isOpen(Hud.Window.BACKPACK)) {
			moneyGained = getMoneyGained();
			//}

			//antiPattern();

			//currentMiningSubScript.execute();

			for (Task task : tasks) {
				if (task.activate()) {
					if (debug) {
						DebugMethods.println(task.getClass().getSimpleName() + ".execute()");
					}
					task.execute();
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		MyMethods.sleep(100, 300);
		return;
	}

	/*private void manageBankCoalBagWithdraw() {
		MyMethods.println("manageBankCoalBagWithdraw()");

		//Item coalbag = myInventory.getItem(COALBAG_ID);
		Item coalbag = ctx.backpack.select().id(COALBAG_ID).poll();
		if (coalbag.valid()) {
			//if (MyMethods.interact(ctx, coalbag, "Empty", "")) {
			if (coalbag.interact("Empty")) {
				MyMethods.sleep(700, 1000);
				if (ctx.backpack.select().id(Ore.COAL.getId()).count() > 0) {
					coalBagCount = 0;
				}
			}
		}
	}	

	private void manageFillCoalBag() {
		if (debug) {
			DebugMethods.println("manageFillCoalBag()");
		}
		if (ctx.backpack.select().id(Ore.COAL.getId()).count() > 0
				&& ctx.backpack.select().count() == 28) {

			//Item coalbag = myInventory.getItem(COALBAG_ID);
			Item coalbag = ctx.backpack.select().id(COALBAG_ID).poll();
			if (coalbag.valid()) {

				int startCoalCount = ctx.backpack.select().id(Ore.COAL.getId()).count();
				//if (MyMethods.interact(ctx, coalbag, "Fill", "")) {
				if (coalbag.interact("Fill")) {
					MyMethods.sleep(1100, 1400);
					coalBagCount += startCoalCount - ctx.backpack.select().id(Ore.COAL.getId()).count();
				}
			}

			return;
		}

	}

	/*private void countInventoryOresAndGemsAsDeposited() {
		if (MyCalculations.getSecondsSince(lastCountedDeposits) > 30) {
			for (OreInfo oreInfo : oreInfoList) {
				oreInfo.increaseBankCountBy(myDepositBox.getCount(oreInfo.getId()));
			}
			for (GemInfo gem : gems) {
				gem.increaseBankCountBy(myDepositBox.getCount(gem.getId()));
			}
			lastCountedDeposits = System.currentTimeMillis();
		}
	}*/

	public void countInventoryOresAndGemsAsBanked() {
		if (MyCalculations.getSecondsSince(lastCountedDeposits) > 30) {
			for (OreInfo oreInfo : oreInfoList) {
				oreInfo.increaseBankCountBy(ctx.backpack.select().id(oreInfo.getId()).count());
			}
			for (GemInfo gem : gems) {
				gem.increaseBankCountBy(ctx.backpack.select().id(gem.getId()).count());
			}
			lastCountedDeposits = System.currentTimeMillis();
		}
	}

	/*public void manageRunModeForSmallMines() {
		if (!ctx.movement.running()
				&& ctx.movement.energyLevel() >= Random.nextInt(15, 30)) {
			ctx.movement.running(true);
			MyMethods.sleep(100, 300);
		}
	}/*

	/*private void antiPattern() {

		if (Random.nextInt(0, 100) == 0) {
			MyMethods.println("checking if camera pitch is true");
			if (ctx.camera.getPitch() < 89) {
				MyMethods.println("setting camera pitch true");
				ctx.camera.
				ctx.camera.setPitch(true);
			}
		}

		int randomNumber = Random.nextInt(0, 285);

		if (randomNumber == 10) {
			MyMethods.println("Randomly setting camera pitch 40-100");
			ctx.camera.setPitch(Random.nextInt(40, 100));
		} else if (randomNumber == 11) {
			MyMethods.println("randomly moving mouse");
			ctx.mouse.move(Random.nextInt(0, 1024), Random.nextInt(0, 768));
			MyMethods.sleep(300, 500);
		}

	}*/

	@Override
	public void messaged(MessageEvent e) {

		if(e.source().isEmpty()) {

			String msg = e.text();

			/*for (Ore2 ore : ores) {
				if (msg.contains(ore.getName().toLowerCase())) {
					ore.incrementCount();
					return;
				}
			}*/ 

			if (msg.contains("You'll need to activate")) {
				useLodestone = false;
			} else if (msg.contains("Your coal bag is already")) {
				coalBagCount = 27;
			} else if (msg.contains("There is no coal in")) {
				coalBagCount = 0;
				//} else if (msg.contains("Oh dear, you are d")) {
				//if (!currentMiningSubScript.getClass().getName().equals("LivingRockCavern")) {
				//MyMethods.println(msg);
				//shutdown();	
				//}
			}

		}
	}

	@Override
	public void stop() {

		if (skillTracker == null) {
			MyMethods.println("stopping");
			return;
		}

		MyMethods.println(displayname + " Stopped after " + MyMethods.formattedTimeSince(startTime));
		MyMethods.println("Levels Gained: " + skillTracker.getLevelsGained());
		MyMethods.println("Experience Gained: " + skillTracker.getExperienceGained());
		MyMethods.println("Experience / Hour: " + MyMethods.formatDouble(skillTracker.getExperiencePerHour()));
	}

	@Override
	public void repaint(Graphics g) {

		Point m = ctx.input.getLocation();
		g.setColor(Color.cyan);
		g.drawRoundRect(m.x - 6, m.y, 15, 3, 5, 5);
		g.drawRoundRect(m.x, m.y - 6, 3, 15, 5, 5);
		g.fillRoundRect(m.x - 6, m.y, 15, 3, 5, 5);
		g.fillRoundRect(m.x, m.y - 6, 3, 15, 5, 5);

		int x = 5, y = 5;

		if (message != null) {

			g.setColor(PaintBackgroundColor);
			g.fillRect(x, y, 200, 21);
			x += 5;

			g.setColor(Color.RED);
			g.setFont(new Font("Tahoma", Font.BOLD, 12));
			g.drawString(message, x, 70);
			return;
		}

		if (startTime == 0) {
			return;
		}

		//draw background
		g.setColor(PaintBackgroundColor);
		g.fillRect(x, y, 129, 168);
		x += 5;
		y += 15;

		g.setColor(Color.white);
		g.setFont(new Font("Tahoma", Font.BOLD, 11));
		g.drawString("Super Miner " + version, x, y);
		y += 15;

		g.setColor(Color.red); 
		g.setFont(new Font("Tahoma", Font.PLAIN, 11));

		g.drawString("Time: " + MyMethods.formattedTimeSince(startTime) , x, y);
		y += 15;

		g.setColor(Color.RED);
		g.setFont(new Font("Tahoma", Font.PLAIN, 11));

		g.drawString("Method: " + miningMethod, x, y);
		y += 15;
		if (skillTracker != null) {
			g.drawString("Current level: " + skillTracker.getCurrentLevel(), x, y);
			y += 15;
			g.drawString("Levels Gained: " + skillTracker.getLevelsGained(), x, y);
			y += 15;
			g.drawString("Current Exp: " + skillTracker.getCurrentExperience(), x, y);
			y += 15;
			g.drawString("Exp Gained: " + skillTracker.getExperienceGained(), x, y);
			y += 15;
			g.drawString("Exp / Hour: " + MyMethods.formatDouble(skillTracker.getExperiencePerHour()), x, y);
			y += 15;
			g.drawString("Exp TNL: " + skillTracker.getExperienceToNextLevel(), x, y);
			y += 15;


			if (skillTracker.getExperienceGained() == 0) {
				g.drawString("Time TNL: ", x, y);
			} else {	
				g.drawString("Time TNL: " + MyMethods.formatTime(skillTracker.getMillisecondsToNextLevel()), x, y);
			}
			y += 7;

			//fill bar
			g.fillRect(x, y, 100, 4);
			g.setColor(Color.GREEN);
			g.fillRect(x, y, (int)Math.round(skillTracker.getPercentOfLevelCompletion()), 4);
			y += 18;
		}

		if (bankingEnabled) {

			for (OreInfo oreInfo : oreInfoList) {
				g.setColor(PaintBackgroundColor);
				g.fillRect(x - 5, y - 7, 129, 15);
				g.setColor(Color.RED);
				g.drawString(oreInfo.getName() + " Mined: " + oreInfo.getCount(), x, y);
				y += 15;
			}

			for (GemInfo gem : gems) {
				g.setColor(PaintBackgroundColor);
				g.fillRect(x - 5, y - 7, 129, 15);
				g.setColor(Color.RED);
				g.drawString(gem.getName() + "s Mined: " + gem.getCount(), x, y);
				y += 15;
			}

			g.setColor(PaintBackgroundColor);
			g.fillRect(x - 5, y - 7, 129, 15);
			g.setColor(Color.RED);
			g.drawString("Money Gained: " + MyMethods.formatDouble(moneyGained), x, y);
			y += 15;

			g.setColor(PaintBackgroundColor);
			g.fillRect(x - 5, y - 7, 129, 15);
			g.setColor(Color.RED);
			g.drawString("Money / Hour: " + MyMethods.formatDouble((double)moneyGained 
					/ MyCalculations.getHoursSince(startTime)), x, y);
			y += 15;

			g.setColor(PaintBackgroundColor);
			g.fillRect(x - 5, y - 7, 129, 15);
			g.setColor(Color.LIGHT_GRAY);
			g.drawString("Click Here for more info", x, y);
			y += 15;

		}

		if (debug) {
			g.setColor(Color.GREEN);

			g.drawString("Animation: " + ctx.players.local().animation(), x, y);
			y += 15;

			g.drawString("inMotion: " + ctx.players.local().inMotion(), x, y);
			y += 15;

			g.drawString("ctx.players.local().animation(): " + ctx.players.local().animation(), x, y);
			y += 15;

			Mine mine = new Mine(ctx, areaInfo);
			GameObject rock = mine.getRock();
			g.drawString("rock.valid(): " + rock.valid(), x, y);
			y += 15;
			g.drawString("rock.tile().distanceTo(ctx.players.local()): " + rock.tile().distanceTo(ctx.players.local()), x, y);
			y += 15;
			g.drawString("MyCalculations.playerIsFacing(ctx.players.local(), rock): " + MyCalculations.playerIsFacing(ctx.players.local(), rock), x, y);

			DebugMethods.drawTileArrayOnMap(ctx, g, areaInfo.tilesToBank(), Color.GREEN);
			GameObject door = ctx.objects.select().id(11714).at(new Tile(3061, 3374, 0)).poll();
			DebugMethods.drawTileArrayOnMap(ctx, g, new Tile[]{door.tile()}, Color.RED);
			door.draw(g);

		}

	}

	private int getMoneyGained() {
		int moneyGained = 0;

		for (OreInfo oreInfo : oreInfoList) {
			oreInfo.updateInventoryCount();
			moneyGained += oreInfo.getCount() * oreInfo.getPrice();
		}

		for (GemInfo gem : gems) {
			gem.updateInventoryCount();
			moneyGained += gem.getCount() * gem.getPrice();
		}

		/*moneyGained += uncutSapphiresMined * uncutSapphirePrice;
		moneyGained += uncutEmeraldsMined * uncutEmeraldPrice;
		moneyGained += uncutRubysMined * uncutRubyPrice;
		moneyGained += uncutDiamondsMined * uncutDiamondPrice;*/

		//granite is not counted when mined
		//moneyGained += granite500gMined * granite500gPrice;
		//moneyGained += granite2KgMined * granite2KgPrice;
		//moneyGained += granite5KgMined * granite5KgPrice;
		return moneyGained;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		Rectangle paint = new Rectangle(5, 55, 127, 343);
		if (e.getComponent() instanceof java.awt.Canvas 
				&& paint.contains(e.getPoint())) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				new ShowMoreGui(ctx);
			} else if (e.getButton() == MouseEvent.BUTTON3) {
				debug = !debug;
			}
		}

	}


	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

}